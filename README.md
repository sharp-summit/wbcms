## 基于ASP.NET MVC开发的cms站点  

- 特点  
  - 轻量级架构  
  - 响应式管理界面  
  - 功能自定义  
  - 自定义扩展字段  
  - 内置URL重写引擎  
- 许可证  
  GNU GENERAL PUBLIC LICENSE Version 3
