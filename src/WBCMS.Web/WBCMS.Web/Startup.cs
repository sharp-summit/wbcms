﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WBCMS.Web.Startup))]
namespace WBCMS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
